port module Main exposing (main)

import Html exposing (..)
import Html.Attributes exposing (class, classList, placeholder, style, id, attribute, value)
import Browser
import Browser.Events
import Json.Decode as D
import Json.Encode as E
import Html.Events.Extra.Mouse as Mouse
import Tuple
import Dict exposing (Dict)
import Array exposing (Array)
import Node exposing (..)
import Rpc exposing (RpcAction(..), NodeEdit(..), decodeRpcAction)

-- MODEL

type alias Model =
    { nodes: Dict NodeKey Node
    , dragState: Maybe DragState
    , connections: List NodeConnection
    , translation: (Float, Float)
    , scale: Float
    }

type alias NodeConnection =
    { start: (NodeKey, String)
    , end: (NodeKey, String)
    }

type alias VisualConnection =
    { start: GraphPos
    , end: GraphPos
    }

type alias DragState =
    { initial: MouseCoord
    , current: MouseCoord
    , mode: DragMode
    }

type DragMode = DraggingNode
    | DraggingWire FloatingWire
    | DraggingBackground

type alias FloatingWire =
    { source: (NodeKey, String)
    , isByHead: Bool
    }

type alias MouseCoord = (Float, Float)

-- PORTS

port renderCanvas : E.Value -> Cmd msg
port receiveCanvasMouseDown : (D.Value -> msg) -> Sub msg
port receiveScrollAmount : (E.Value -> msg) -> Sub msg
port receiveRpcAction : (E.Value -> msg) -> Sub msg

encodeCanvas : Model -> E.Value
encodeCanvas model =
    E.object
        [ ("connections", E.list encodeConnection model.connections)
        , ("floatingWire", encodeFloatingWire model.dragState)
        , ("translation", encodeTranslation model)
        , ("scale", E.float model.scale)
        ]

encodeConnection : NodeConnection -> E.Value
encodeConnection { start, end } =
    E.object
        [ ("start", encodeWireSource start )
        , ("end", encodeWireSource end)
        ]

encodeFloatingWire : Maybe DragState -> E.Value
encodeFloatingWire dragState =
    dragState
    |> Maybe.andThen
        (\d ->
            case d.mode of
                DraggingWire fw ->
                    Just
                        (E.object
                            [ ("source", encodeWireSource fw.source)
                            , ("isByHead", E.bool fw.isByHead)
                            , ("mousePos", encodeFloatPos d.current)
                            ]
                        )
                _ -> Nothing
        )
    |> Maybe.withDefault E.null

encodeWireSource : (NodeKey, String) -> E.Value
encodeWireSource (refKey, socket) =
    E.list E.string [refKey, socket]

encodeTranslation : Model -> E.Value
encodeTranslation = currentTranslation >> encodeFloatPos

encodeFloatPos : MouseCoord -> E.Value
encodeFloatPos (x, y) =
    E.object
        [ ("x", E.float x)
        , ("y", E.float y)
        ]

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onMouseUp handleMouseUp
        , Browser.Events.onKeyDown handleKeyDown
        , receiveScrollAmount
            (\amount ->
                amount
                |> D.decodeValue D.float
                |> Result.withDefault 0.0
                |> ZoomViewport
            )
        , receiveCanvasMouseDown
            (decodeMousePos >> SelectBackground)
        , case model.dragState of
            Just state -> Browser.Events.onMouseMove handleMouseMove
            Nothing -> Sub.none
        , receiveRpcAction (decodeRpcAction >> RpcResponse)
        ]

handleMouseUp : D.Decoder Msg
handleMouseUp = D.succeed EndDrag

handleKeyDown : D.Decoder Msg
handleKeyDown =
    (D.field "key" D.string)
    |> D.andThen
        (\key ->
            if key == "Escape"
            then D.succeed Deselect
            else D.fail "key press ignored"
        )

handleMouseMove : D.Decoder Msg
handleMouseMove =
    D.map
        (.clientPos >> MouseMove)
        Mouse.eventDecoder

decodeMousePos : D.Value -> Result D.Error MouseCoord
decodeMousePos =
    let
        unwrap = Maybe.withDefault 0
    in
        D.decodeValue
            (D.map
                (\c -> (unwrap (Array.get 0 c), unwrap (Array.get 1 c)))
                (D.array D.float))

-- INIT

init : () -> (Model, Cmd Msg)
init _ =
    let
        model =
            { nodes = Dict.empty
            , dragState = Nothing
            , connections = []
            , translation = (0, 0)
            , scale = 1.0
            }
    in
        ( model
        , renderCanvas (encodeCanvas model)
        )

-- VIEW

view : Model -> Html Msg
view model =
    div []
        (Dict.values
            (Dict.map (viewNode model) model.nodes))

viewNode : Model -> NodeKey -> Node -> Html Msg
viewNode model refKey ({ props, active, position } as node) =
    let
        (leftPixels, topPixels) = getNodePosition model node
        left = (String.fromInt leftPixels) ++ "px"
        top = (String.fromInt topPixels) ++ "px"
    in
    div
        [ id ("node-ref-" ++ refKey)
        , class "node"
        , classList [("active", active), ("error", List.length props.errors > 0)]
        , style "left" left, style "top" top
        , style "transform" ("scale(" ++ (String.fromFloat model.scale) ++ ")")
        ]
        [ header
            [ class "node-header"
            , Mouse.onDown
                (\e ->
                    SelectNode { key = refKey, pos = e.clientPos, shiftKey = e.keys.shift }
                )
            ]
            [ div [ class "node-header-gradient" ]
                [ h2 [] [ text props.title ]
                ]
            ]
        -- put this here so we can use the .node-header:hover + .node-error selector:
        , if List.length props.errors > 0
            then div [ class "node-error" ]
                (List.map
                    (\s -> p [] [ strong [] [ text "Error: " ], text s ])
                    props.errors)
            else text ""
        , div [ class "node-body" ]
            [ case props.expression of
                Just expr ->
                    div [ class "node-exprs", class "column" ]
                        [ input [ placeholder "enter an expression", value expr ] []
                        ]
                Nothing ->
                    text ""
            , div [ class "node-io", class "row" ]
                [ div [ class "node-inputs", class "column"]
                    (List.map (viewSocket refKey True) props.inputs)
                , div [ class "node-outputs", class "column" ]
                    (List.map (viewSocket refKey False) props.outputs)
                ]
            ]
        ]

viewSocket : NodeKey -> Bool -> NodeSocket -> Html Msg
viewSocket refKey isInput socket =
    let
        label = span [] [ text socket.name ]
        socketAttrs =
            [ class "node-socket"
            , styleSocket socket.kind
            ]
    in
        div
            [ class "node-io-item"
            , class (if isInput then "input" else "output")
            , attribute "data-socket" socket.name
            , Mouse.onDown
                (\e -> SelectSocket
                    (SelectSocketProps isInput refKey socket.name e.clientPos)
                )
            , makeSocketOnUpHandle isInput (refKey, socket.name)
            ]
            (if isInput
            then
                [ div socketAttrs []
                , label
                ]
            else
                [ label
                , div socketAttrs []
                ])

makeSocketOnUpHandle : Bool -> (NodeKey, String) -> Attribute Msg
makeSocketOnUpHandle isInput socket =
    Mouse.onWithOptions
        "mouseup"
        { stopPropagation = True, preventDefault = False }
        (\_ -> CreateConnection { socket = socket, isInput = isInput })

styleSocket : String -> Html.Attribute msg
styleSocket kind =
    Dict.fromList
        [ ("collection", "rgb(156, 171, 17)")
        , ("vec", "rgb(200, 0, 0)")
        ]
    |> Dict.get kind
    |> Maybe.withDefault "pink"
    |> style "background-color"

-- get the node's position in *screen space*, with translation applied
getNodePosition : Model -> Node -> GraphPos
getNodePosition model node =
    model.dragState
    |> Maybe.map
        (\drag ->
            case drag.mode of
                DraggingNode -> if node.active then mouseDelta model.scale drag else (0, 0)
                _ -> (0, 0)
        )
    |> Maybe.withDefault (0, 0)
    |> sumVec2 (negateVec2 (currentTranslation model))
    |> sumVec2 (floatVec2 node.position)
    |> scaleVec2 model.scale
    |> roundVec2

scaleVec2 : number -> (number, number) -> (number, number)
scaleVec2 factor (x, y) =
    (factor * x, factor * y)

floatVec2 : (Int, Int) -> (Float, Float)
floatVec2 (x, y) =
    (toFloat x, toFloat y)

sumVec2 : (number, number) -> (number, number) -> (number, number)
sumVec2 (x1, y1) (x2, y2) =
    (x1 + x2, y1 + y2)

negateVec2 : (number, number) -> (number, number)
negateVec2 (x, y) = (-x, -y)

roundVec2 : (Float, Float) -> (Int, Int)
roundVec2 d =
    Tuple.mapBoth round round d

mouseDelta : Float -> DragState -> (Float, Float)
mouseDelta factor { current, initial } =
    let
        (x, y) = current
        (ix, iy) = initial
    in
        scaleVec2 (1 / factor) (x - ix, y - iy)

-- update

type Msg = SelectNode SelectNodeProps
    | SelectSocket SelectSocketProps
    | SelectBackground (Result D.Error (Float, Float))
    | Deselect
    | ZoomViewport Float
    | MouseMove (Float, Float)
    | EndDrag
    | CreateConnection { socket: (NodeKey, String), isInput: Bool }
    | RpcResponse (Result D.Error RpcAction)

type alias SelectNodeProps =
    { key: NodeKey
    , pos: (Float, Float)
    , shiftKey: Bool
    }

type alias SelectSocketProps =
    { isInput: Bool
    , key: NodeKey
    , socket: String
    , pos: (Float, Float)
    }

update : Msg -> Model -> (Model, Cmd Msg)
update msg oldModel =
    let
        model = case msg of
            SelectNode { key, pos, shiftKey } ->
                { oldModel
                | nodes =
                    if shiftKey
                    then Dict.update
                        key
                        (Maybe.andThen (\n -> Just { n | active = True }))
                        oldModel.nodes
                    else if -- newly selected node was active
                        Dict.get key oldModel.nodes
                        |> Maybe.map (\n -> n.active)
                        |> Maybe.withDefault False
                    then -- do nothing
                        oldModel.nodes
                    else -- the only thing active should be the selected node
                        Dict.map
                        (activateNode key)
                        oldModel.nodes
                , dragState = beginDrag pos DraggingNode
                }
            SelectSocket { isInput, key, socket, pos } ->
                if not isInput
                then -- simple case: just create a new floating node
                    { oldModel
                    | dragState =
                        beginDrag
                            pos
                            (DraggingWire { source = (key, socket), isByHead = True })
                    }
                else -- complex case: detach existing node else work backwards
                    let
                        oldConnection =
                            List.filter
                                (\c -> c.end == (key, socket))
                                oldModel.connections
                            |> List.head
                    in
                        case oldConnection of
                            Just { start } -> -- existing connection, detach and modify it
                                { oldModel
                                | dragState =
                                    beginDrag
                                        pos
                                        (DraggingWire { source = start, isByHead = True })
                                , connections =
                                    List.filter
                                        (\c -> c.end /= (key, socket))
                                        oldModel.connections
                                }
                            Nothing -> -- no connection, we are dragging backwards
                                {oldModel
                                | dragState =
                                    beginDrag
                                        pos
                                        (DraggingWire { source = (key, socket), isByHead = False })
                                }
            SelectBackground decodeResult ->
                { oldModel
                | dragState =
                    case decodeResult of
                        Ok mousePos ->
                            beginDrag
                                mousePos
                                DraggingBackground
                        Err _ -> beginDrag (0, 0) DraggingNode
                }
            ZoomViewport amount ->
                { oldModel
                | scale =
                    clamp
                        0.2
                        2
                        (oldModel.scale + -(amount / abs amount) * 0.1)
                }
            Deselect ->
                { oldModel
                | nodes =
                    Dict.map (\_ node -> { node | active = False }) oldModel.nodes
                }
            MouseMove (x, y) ->
                { oldModel
                | dragState = Maybe.map
                    (\d -> { d | current = (x, y) })
                    oldModel.dragState
                }
            EndDrag ->
                commitDragState oldModel
            CreateConnection props ->
                commitConnection oldModel props
            RpcResponse res ->
                res
                |> Result.map
                    (rpcUpdate oldModel)
                |> Result.withDefault oldModel
    in
    ( model
    , Cmd.batch [ renderCanvas (encodeCanvas model) ]
    )

beginDrag : (Float, Float) -> DragMode -> Maybe DragState
beginDrag (x, y) mode =
    Just
        { initial = (x, y)
        , current = (x, y)
        , mode = mode
        }

activateNode : NodeKey -> NodeKey -> Node -> Node
activateNode newlySelected key node =
    let
        active = newlySelected == key
    in
        { node | active = active }

commitDragState : Model -> Model
commitDragState model =
    case model.dragState of
        Nothing -> model
        Just dragState ->
            case dragState.mode of
                DraggingNode ->
                    { model
                    | dragState = Nothing
                    , nodes = Dict.map (commitDragStateToNode model.scale dragState) model.nodes
                    }
                -- if this fires, the specific link connection handler did not fire
                -- i.e. no connection was made
                DraggingWire fw ->
                    { model
                    | dragState = Nothing
                    }
                DraggingBackground ->
                    { model
                    | dragState = Nothing
                    , translation = currentTranslation model
                    }

currentTranslation : Model -> (Float, Float)
currentTranslation model =
    case model.dragState of
        Nothing -> model.translation
        Just dragState ->
            case dragState.mode of
                DraggingBackground -> dragState
                    |> mouseDelta model.scale
                    |> negateVec2
                    |> sumVec2 model.translation
                _ -> model.translation

commitDragStateToNode : Float -> DragState -> NodeKey -> Node -> Node
commitDragStateToNode scale dragState _ node =
    if node.active
    then
        { node
        | position =
            roundVec2 (sumVec2 (floatVec2 node.position) (mouseDelta scale dragState))
        }
    else node

commitConnection : Model -> { socket: (NodeKey, String), isInput: Bool } -> Model
commitConnection model { socket, isInput } =
    case model.dragState of
        Nothing -> model
        Just dragState ->
            case dragState.mode of
                DraggingNode -> model
                DraggingBackground -> model
                DraggingWire fw ->
                    if fw.isByHead == isInput
                    then
                        let
                            connection = linkSocket fw.source (isInput, socket)
                            connections =
                                List.filter
                                    (\c -> c.end /= connection.end)
                                    model.connections
                        in
                            { model
                            | connections = connection :: connections
                            , dragState = Nothing
                            }
                    else -- the connection is invalid, ignore it
                        { model
                        | dragState = Nothing
                        }

rpcUpdate : Model -> RpcAction -> Model
rpcUpdate model action =
    case action of
        OpenFile edits ->
            let
                nodes =
                    List.foldl
                        applyNodeEdit
                        model.nodes
                        edits.nodeEdits
                -- TODO: connections = ...
            in
                { model | nodes = nodes }

applyNodeEdit : NodeEdit -> Dict NodeKey Node -> Dict NodeKey Node
applyNodeEdit edit nodes =
    case edit of
        CreateNode (key, node) ->
            Dict.insert
                key
                node
                nodes

-- assume the pairing is valid, e.g. a is an input and b is an output or vice versa
-- use bIsInput to determine which way to go
linkSocket : (NodeKey, String) -> (Bool, (NodeKey, String)) -> NodeConnection
linkSocket a (bIsInput, b) =
    if bIsInput
    then { start = a, end = b }
    else { start = b, end = a }

-- MAIN

main = Browser.element { init = init, view = view, update = update, subscriptions = subscriptions }
