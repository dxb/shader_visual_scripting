module Rpc exposing (..)

import Json.Decode as D
import Node exposing (..)

type RpcAction =
    OpenFile GraphEdits

type alias GraphEdits =
    { nodeEdits: List NodeEdit
    , version: Maybe Int
    }

type NodeEdit =
    CreateNode (NodeKey, Node)

decodeRpcAction : D.Value -> Result D.Error RpcAction
decodeRpcAction val =
    let
        method = (D.decodeValue (D.field "method" D.string) val)
    in
        case method of
            Ok "open_file" ->
                Result.map
                    OpenFile
                    (D.decodeValue
                        (D.field "response"
                            (decodeRpcResult decodeOpenFile))
                        val)
            _ ->
                D.decodeValue
                    (D.fail "Unknown RPC response type")
                    val

decodeRpcResult : D.Decoder a -> D.Decoder a
decodeRpcResult obj =
    D.field "result" obj

decodeOpenFile =
    D.map2
        GraphEdits
        (D.field "node_edits" (D.list decodeNodeEdit))
        -- (D.field "edge_edits" D.list EdgeEdit)
        (D.field "version" (D.maybe D.int))

-- the action is inside of a "result" field
decodeNodeEdit : D.Decoder NodeEdit
decodeNodeEdit =
    D.oneOf
        [ decodeCreateNode
        ]

decodeCreateNode : D.Decoder NodeEdit
decodeCreateNode =
    D.map
        CreateNode
        (D.field "Create"
            (D.map3
                (\a b c -> (a, { props = b, position = c, invalidExpr = False, active = False }))
                (D.field "id" (D.map String.fromInt D.int))
                (D.field "node" decodeDescriptor)
                (D.field "position" decodeGraphPos)))

decodeGraphPos =
    D.map2 Tuple.pair
        (D.index 0 D.int)
        (D.index 1 D.int)

decodeDescriptor : D.Decoder NodeDescriptor
decodeDescriptor =
    D.map5
        NodeDescriptor
        (D.field "inputs" (D.list decodeSocket))
        (D.field "outputs" (D.list decodeSocket))
        (D.field "name" D.string)
        (D.field "expr" (D.maybe D.string))
        (D.field "errors" (D.list D.string))

decodeSocket =
    D.map
        (\s -> { kind = "any", name = s})
        D.string
