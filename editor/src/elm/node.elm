module Node exposing (..)

type alias NodeKey = String
type alias GraphPos = (Int, Int)

type alias Node =
    { props: NodeDescriptor
    , active: Bool
    , position: GraphPos
    }

type alias NodeDescriptor  =
    { inputs: List NodeSocket
    , outputs: List NodeSocket
    , title: String
    , expression: Maybe String
    , errors: List String
    }

type alias NodeSocket =
    { kind: String
    , name: String
    }
