/*
const ffi = require('ffi')
const ref = require('ref')
const Struct = require('ref-struct')

const u8Ptr = ref.refType(ref.types.uint8)
const usize = ref.types.size_t

const JsonResult = Struct({
    buffer: u8Ptr,
    length: usize,
    capacity: usize
})

const shagrac_native = ffi.Library('build/shagrac.dll', {
    alloc_parsed_expression: [JsonResult, [u8Ptr, usize]],
    shagrac_free: ['void', [JsonResult]]
})

function parseExpression(expr) {
    const input = Buffer.from(expr, 'utf8')
    const r = shagrac_native.alloc_parsed_expression(input, input.length)
    const output = ref.reinterpret(r.buffer, r.length, 0)
    shagrac_native.shagrac_free(r)
    return output.toString('utf8')
}
*/

function parseExpression(expr) {
    return ":("
}

module.exports = { parseExpression }
