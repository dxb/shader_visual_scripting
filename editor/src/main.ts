import { app, BrowserWindow } from 'electron';
import * as child_process from 'child_process';
import { LanguageClient, ConsoleLogger, LogLevel } from './lang-client/client';
import { RpcMethod } from './lang-client/protocol';

let mainWindow: BrowserWindow | null = null;

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 1000,
        height: 700,
        webPreferences: {
            nodeIntegration: true
        }
    })

    const compileResult = child_process.spawnSync('elm', [
        'make', 'src/elm/main.elm', '--output', 'build/elm.js'
    ], { stdio: "inherit" })
    const successStatus = process.platform === 'win32' ? 1 : 0
    if (compileResult.status !== successStatus) {
        console.error("===\nAn error occured while compiling elm code, see above.")
    }

    const serverCompileResult = child_process.spawnSync(
        'cargo',
        ['build'],
        { stdio: 'pipe', cwd: '../shagrac' }
    );
    if (serverCompileResult.status !== 0) {
        console.error('===\nAn error occured while compiling rust code:');
        const out = serverCompileResult.stderr.toString('utf8');
        console.error(out);
    }

    console.log('===');

    mainWindow.loadFile('src/views/graph.html');

    mainWindow.on('closed', function () {
        mainWindow = null;
    })

    const rendererMessages = new IpcBuffer(false);
    mainWindow.webContents.on('did-finish-load', () => {
        rendererMessages.ready = true;
    });

    const client = new LanguageClient({
        port: 62007,
        reconnectTimeout: 3000,
        server_args: ['--server'],
        server_executable: '../shagrac/target/debug/shagrac.exe',
    }, new ConsoleLogger(LogLevel.Info));

    client.start(() => {
        const r = { method: RpcMethod.OpenFile, params: { path: '../shagrac/tests/basic.json' } }
        client.request(r, (err, res) => {
            if (err) {
                client.logError(err);
                return;
            }
            rendererMessages.send('rpc-response', { method: RpcMethod.OpenFile, response: res });
        })
    });
}

/*
class LanguageServerClient extends EventEmitter {
constructor() {
super()
// an array of commands to be executed
this.commands = []
// an array of handlers; if empty, the idle handler is used (see handleMessage)
this._state = []
}

// Queue up a command
_trigger(command) {
this.commands.push(command)
this._execute()
}

_execute() {
if (this._state.length === 0 && this.commands.length > 0) {
const { request, handlers } = this.commands.shift()
this._state = handlers || []
this.emit('request', request)
}
}

handleMessage(message) {
if (message.type === 'Error') {
this.emit('ignored', message)
this._state = []
this._execute()
return
}

if (this._state.length === 0) {
// idle hanlder
this.emit('ignored', message)
} else {
// pop a handler off and use that
const handler = this._state.shift()
// handlers may return commands
const result = handler.call(this, message)
if (result && result.request) {
this._trigger(result)
}
}
// continue running commands if we end up idle
this._execute()
}

handshake() {
this._trigger({
request: { type: 'Init' },
handlers: [
expectResponse('Init', 'Ack'),
]
})
}

openFile(path) {
path = resolve(path)
this._trigger({
request: { type: 'OpenFile', path },
handlers: [
expectResponse('Ack'),
(message) => {
if (message.type !== 'LoadGraph') {
return this.emit('fatal', 'Expected LoadGraph response')
}
console.log('Load graph result:', message)
this.emit('request', { type: 'Ack' })
}
]
})
}
}

// Create a handler that expects type, emits fatal if not found, otherwise issues request
function expectResponse(type, requestType) {
return function(message) {
if (message.type !== type)
return this.emit('fatal',
'Expected a response of type ' + type + '; instead got type ' + message.type,
)

console.log('Got expected response of type', type)

if (requestType)
this.emit('request', { type: requestType })
}
}*/

app.on('window-all-closed', function () {
    // For mac
    if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) createWindow()
})

app.on('ready', createWindow)

class IpcBuffer {
    private _queue: Array<[string, any]>;
    private _ready: boolean;

    constructor(ready: boolean) {
        this._ready = ready;
        this._queue = [];
    }

    get ready() { return this._ready };

    set ready(ready: boolean) {
        this._ready = ready;
        if (ready) {
            for (const [c, m] of this._queue) {
                this.send(c, m);
            }
            this._queue = []
        }
    }

    send(channel: string, message: any) {
        if (this.ready) {
            mainWindow!.webContents.send(channel, message);
        } else {
            this._queue.push([channel, message]);
        }
    }
}