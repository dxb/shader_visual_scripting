import { RpcError, RpcRequest } from "./protocol";

export type RequestId = number;

export class RpcEncoder {
    private _writer: NodeJS.WritableStream;
    private _id: number;

    constructor(writer: NodeJS.WritableStream) {
        this._writer = writer;
        this._id = 0;
    }

    request<R extends RpcRequest>(req: R): RequestId {
        const id = this.useId();
        const msg = { id, jsonrpc: '2.0', ...req };
        sendMessage(this._writer, msg);
        return id;
    }

    // TODO: notify() { }

    /* respond to a request with an error message */
    error(err: RpcError) {
        sendMessage(this._writer, err);
    }

    private useId() {
        return this._id++;
    }
}

function sendMessage(client: NodeJS.WritableStream, message: any) {
    message = { jsonrpc: '2.0', ...message };
    message = JSON.stringify(message)
    const buffer = Buffer.from(message, 'utf8')
    // the rust side expects a LE usize, e.g. 8 bytes, but Number doesn't actually have
    // the precision to do 8, so we only send 4 (n.b., we could send up to 6)
    const contentLength = Buffer.alloc(8, 0)
    contentLength.writeUInt32LE(buffer.length, 0)
    client.write(contentLength)
    client.write(buffer)
}
