export interface RpcRequest {
    method: RpcMethod;
    _response?: RpcResult<any>;
}

export enum RpcMethod {
    Initialize = 'initialize',
    OpenFile = 'open_file',
}

export interface InitializeRequest extends RpcRequest {
    method: RpcMethod.Initialize;
    _response?: InitializedResponse;
}

export interface OpenFile {
    method: RpcMethod.OpenFile,
    params: {
        path: string;
    }
}

type InitializedResponse = RpcResult<true>;

export type RpcResponse<T> = RpcResult<T> | RpcError;

export interface RpcResult<T> {
    id: number;
    result: T;
}

export interface RpcError {
    id: number;
    error: {
        code: ErrorCode;
        message: string;
        data?: any;
    };
}

enum ErrorCode {
    ParseError = -32700,
    InvalidRequest = -32600,
    MethodNotFound = -32601,
    InvalidParameters = -32602,
    InternalError = -32603,
    // -32000 to -32099 impl defined server errors
}
