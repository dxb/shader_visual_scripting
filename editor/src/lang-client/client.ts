import * as child_process from 'child_process'
import * as net from 'net'
import { RpcDecoder } from './decoder';
import { RpcEncoder, RequestId } from './encoder';
import { RpcResponse, RpcResult, RpcError, RpcMethod, RpcRequest, InitializeRequest } from './protocol';

interface ServerOptions {
    server_executable: string;
    server_args: string[];
    port: number;
    reconnectTimeout: number,
}

interface Logger {
    logError(source: LogSource, msg: string): void;
    logWarning(source: LogSource, msg: string): void;
    logInfo(source: LogSource, msg: string): void;
}

export enum LogLevel {
    Error = 'error',
    Warning = 'warning',
    Info = 'info',
}

enum LogSource {
    Client = 'client',
    Server = 'server',
}

export class ConsoleLogger implements Logger {
    private level: LogLevel;

    constructor(logLevel: LogLevel) {
        this.level = logLevel;
    }

    private header(source: LogSource): string {
        return source != LogSource.Client
            ? source.toUpperCase() + ': '
            : ''
    }

    logError(source: LogSource, msg: string): void {
        console.error(this.header(source) + msg)
    }

    logWarning(source: LogSource, msg: string): void {
        if (this.level === LogLevel.Info || this.level === LogLevel.Warning)
            console.warn(this.header(source) + msg)
    }

    logInfo(source: LogSource, msg: string): void {
        if (this.level === LogLevel.Info)
            console.log(this.header(source) + msg)
    }
}

export class LanguageClient {
    private _serverOptions: ServerOptions;
    private _clientState: ClientState;
    private _process: child_process.ChildProcess | null;
    private _connection: Connection | null;
    private _logger: Logger;
    private _handlers: { [index: number]: HandlerCallback };

    constructor(options: ServerOptions, logger: Logger) {
        this._serverOptions = options;
        this._clientState = ClientState.Stopped;
        this._process = null;
        this._connection = null;
        this._logger = logger;
        this._handlers = {};
    }

    public start(connectedCallback?: () => void) {
        if (this._connection)
            return;

        this._logger.logInfo(LogSource.Client, 'Connecting to language server');
        const child = child_process.spawn(
            this._serverOptions.server_executable,
            this._serverOptions.server_args,
            { stdio: 'pipe' }
        );
        const iopipe = (element: Logger, method: 'logError' | 'logInfo' | 'logWarning') =>
            (data: any) => {
                if (data && data.length > 0) {
                    const s = data.toString('utf8').trim();
                    element[method](LogSource.Server, s);
                }
            };
        child.stdout.on('data', iopipe(this._logger, 'logInfo'));
        child.stderr.on('data', iopipe(this._logger, 'logError'));
        this._clientState = ClientState.Connecting;
        this._process = child;
        child.on('exit', (status, signal) => { this.handleServerExit(status, signal) })
        this._connection = createConnection(
            this._serverOptions.port,
            (err) => this.handleError(err),
            () => this.handleClose(),
            () => {
                this.request(
                    { method: RpcMethod.Initialize },
                    (err, res) => {
                        if (err) {
                            // TODO: restart the server?
                            this._logger.logError(LogSource.Client, 'Initialize failed');
                            return;
                        }
                        if (res!.result) {
                            this._clientState = ClientState.Connected;
                            if (connectedCallback)
                                connectedCallback()
                        }
                    }
                );
            }
        );
        this.listenConnection();
    }

    public logError(err: RpcError) {
        this._logger.logError(LogSource.Client, `Unhandled error message: ${JSON.stringify(err)}`)
    }

    public request<R extends RpcRequest>(req: R, cb: HandlerCallback) {
        if (!this._connection)
            throw new Error('Cannot make a request before starting the server');

        if (this._clientState !== ClientState.Connected && req.method !== RpcMethod.Initialize) {
            this._logger.logWarning(LogSource.Client, `Attempted to send request ${req} before initialized`);
            return;
        }

        const id = this._connection.encoder.request(req);
        this.registerHandler(id, cb);
    }

    public registerHandler(id: RequestId, cb: HandlerCallback) {
        this._handlers[id] = cb;
        // TODO: clear handlers on timeout
    }

    public stop() {
        // TODO
        this._clientState = ClientState.Stopping;
    }

    private listenConnection() {
        if (!this._connection)
            return;

        this._connection.decoder.on('response', (r) => {
            if (r === undefined) {
                return;
            }
            const handler = this._handlers[r.id];
            if (handler) {
                if ((r as RpcError).error) {
                    handler((r as RpcError));
                } else if ((r as RpcResult<any>).result) {
                    handler(null, (r as RpcResult<any>));
                }
            }
        });
        this._connection.decoder.on('notification', () => {
            console.log('Got notification');
        });
        this._connection.decoder.on('request', () => {
            console.log('Got request');
        });
    }

    private handleServerExit(status: number | null, signal: string | null) {
        this._logger.logError(LogSource.Client, `Server process exited with status ${status} from signal ${signal}`);
        if (this._process) {
            this._process = null;
        }
    }

    private handleError(err: any) {
        this._logger.logError(LogSource.Client, `Language server encountered an error: ${err.message}`);
    }

    private handleClose() {
        this._logger.logWarning(LogSource.Client, 'The server process ended unexpectedly.');
        if (this._process) {
            this._logger.logInfo(LogSource.Client, 'Attempting to kill server process');
            this._process.kill();
        }
        this._connection = null;
        this._process = null
        this._clientState = ClientState.Stopped;

        this._logger.logInfo(
            LogSource.Client,
            `Trying to reconnect again in ${this._serverOptions.reconnectTimeout} ms`
        );
        setTimeout(() => {
            if (!this._process) {
                this.start();
            }
        }, this._serverOptions.reconnectTimeout);
    }
}

type HandlerCallback = (err: RpcError | null, res?: RpcResult<any>) => void;

enum ClientState {
    Stopped = 'stopped',
    Stopping = 'stopping',
    Connecting = 'connecting',
    Connected = 'connected',
}

interface Connection {
    decoder: RpcDecoder;
    encoder: RpcEncoder;
}

function createConnection(
    port: number,
    handleError: (err: any) => void,
    handleClose: () => void,
    connectCallback?: () => void,
): Connection {
    const client = new net.Socket()

    client.on('error', handleError)
    client.on('close', handleClose)

    /*
    const appClient = new LanguageServerClient(client)
    appClient.on('ignored', (message) => {
        console.log('The language server sent an unexpected message:', message)
    })
    appClient.on('fatal', (message) => {
        console.log('Closing the language server. Fatal error occured:', message)
        client.end()
    })
    appClient.on('request', (message) => {
        console.log('Sending out response', message)
        sendMessage(client, message)
    })*/

    const decoder = new RpcDecoder(client);
    const encoder = new RpcEncoder(client);

    if (connectCallback)
        client.on('connect', connectCallback);

    client.connect(port, 'localhost')

    return {
        decoder,
        encoder,
    }
}
