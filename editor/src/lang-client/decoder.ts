import { EventEmitter } from "events";
import { RpcError, RpcResponse, RpcResult } from './protocol';

export class RpcDecoder extends EventEmitter {
    private _queue: Buffer[];
    private _state: DecodeState;

    constructor(reader: NodeJS.ReadableStream) {
        super();
        this._queue = [];
        this._state = { type: 'header' }

        const handleData = (data: Buffer | null) => {
            if (data)
                this._queue.push(data);

            if (this._state.type === 'header') {
                const result = readFromQueue(8, this._queue);
                if (result === null) {
                    return;
                }
                this._state = {
                    type: 'body',
                    contentLength: result.readInt32LE(0),
                };
                // TODO: Throw error if high bits are non zero

                // in case we read a lot of information, attempt to continue parsing
                handleData(null);
            } else {
                const contentLength = this._state.contentLength;
                const result = readFromQueue(contentLength, this._queue);
                if (result === null) {
                    return;
                }
                this._state = { type: 'header' };

                const message = JSON.parse(result.toString('utf8'))
                if (message.result || message.error) {
                    this.emit('response', message);
                } else if (message.method) {
                    if (message.id === undefined || message.id === null) {
                        this.emit('notification');
                    } else {
                        this.emit('request');
                    }
                }
            }
        };

        reader.on('data', handleData);
    }

    on(event: "request", listener: () => void): this;
    on(event: "response", listener: (r: RpcResponse<any>) => void): this;
    on(event: "notification", listener: () => void): this;
    on(event: string, listener: (...args: any[]) => void): this {
        this.addListener(event, listener);
        return this;
    }
}

type DecodeState = DecodeHeader | DecodeBody;

interface DecodeHeader {
    type: 'header';
}

interface DecodeBody {
    type: 'body';
    contentLength: number;
}

function readFromQueue(bytes: number, queue: Buffer[]): Buffer | null {
    const buffered = queue.reduce((sum, buf) => sum + buf.length, 0);
    if (bytes > buffered) {
        return null;
    }
    if (queue[0].length === bytes) {
        return queue.shift()!;
    } else if (queue[0].length > bytes) {
        // yank what is needed from the first element
        const result = queue[0].slice(0, bytes);
        queue[0] = queue[0].slice(bytes);
        return result;
    } else {
        // remove elements until we have what we need
        const result = Buffer.alloc(bytes, 0);
        while (bytes > 0) {
            const upcomingBytes = queue[0].length;
            if (upcomingBytes > bytes) {
                queue[0].copy(result, result.length - bytes, 0, bytes);
                queue[0] = queue[0].slice(bytes);
            } else {
                const popped = queue.shift();
                if (!popped) {
                    throw new Error("The queue was empty despite there being enough bytes");
                }
                popped.copy(result, result.length - bytes);
            }

            bytes -= upcomingBytes
        }
        return result
    }
  }
