# Rough Procedure

 - Load graph
 - Generate Sockets
 - Generate Abstract Graph Representation
 - Determine Vertex Shader
 - Determine Fragment Shader dependencies
 - Generate Control Flow Graphs
 - Remove dead code
 - Determine required loops
 - Codegen

# Nodes



# Expressions

## Types

 - f32, {u|i}{32|16|8}
 - tuples
 - list {T}{N}

## Arithmetic Operators

```
+, -, *, /, ** (for pow), %
&, |, ^, ~, >>, << (bitwise)
```

Intrinsics that need to be kept in mind (for codegen):

```
log2, exp, exp2
```

Comparisons: `==, !=, <, >, >=, <=`

## Built in functions

```
numeric
    abs, clamp, min/max, radians/degrees
    floor, ceil, fract
vector
    mix
    step
    smoothstep
    length
    distance
    cross
    normalize
    reflect
    refract
matrix
    determinant
    inverse

boolean
    &&, ||, !
```

# SPIR-V Intrinsics

trig functions
	standard funcs and arc funcs
	hyperbolic & arc hyperbolic
	atan2
number
	round
	trunc
	abs (fabs and sabs)
	sign (fsign and ssign)
	floort, ceil, fract
	radians & degrees
	{F|S|U}Min/Max
	{F|S|U}Clamp
	FindILsb, Find{U|S}Msb
	NMin - assumes NaN > +inf (vv for NMax)
	NClamp - defnd in terms of NMin and NMax
algebra
	pow
	exp, exp2 (2^x)
	log (ln)
	log2 (log_2)
	sqrt, invsqrt (1/sqrt)
	multiplyadd (fma)
	frexp (solves x = y * 2^exp for y and exp) & ldexp
vector
	FMix (linear blend btwn a, b using alpha)
	Step(edge, x) - 0 if x < edge else 1
	Smoothstep
	Length
	Distance
	Cross
	Normalize
	FaceForward(N, I, Nref)
		tests if vec I is pointing in dir of Nref
		(e.g. dot product is > 0)
		N if so, else -N
	Reflect
	Refract
	Modf
matrix
	determinant
	inverse
compression
	{Un}Pack{S|U}norm{4x8|2x16}
		packs normalized vec4 or vec2 into
		8bit or 16bit int vectors
	{Un}PackHalf2x16
		f32 => f16 => embededded in {i|u}32
	{Un}PackDouble2x32 - not sure
