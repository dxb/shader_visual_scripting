pub mod server;
mod error;
mod expr;
mod analysis;
mod rpc;
mod graph;
mod hir;

use crate::expr::expression;
use nom::IResult;

pub fn debug_expression(s: &str) -> IResult<&str, String> {
    let (input, expr) = expression(s)?;
    Ok((input, format!("{:?}", expr)))
}
