use crate::error::ParseNumberError;

use nom::{
    IResult,
    bytes::complete::{tag, take_while, take_while1, take_while_m_n},
    multi::{fold_many0, separated_list, separated_nonempty_list},
    combinator::{map, map_res},
    branch::alt,
    sequence::{pair, delimited, preceded, terminated},
    ws,
    named,
};

use std::fmt;

#[derive(Debug, Clone)]
pub enum Expression<'a> {
    BinOp(BinOp<'a>),
    Identifier(&'a str),
    Literal(Literal),
    FuncApplication(FuncApplication<'a>),
    // TODO: remove Tuple type, unnecessary
    Tuple(Tuple<'a>),
    List(Vec<Expression<'a>>),
}

pub trait ExprVisitor<'a> where Self: Sized {
    fn bin_op(&mut self, op: &BinOp<'a>) {
        op.lhs.accept(self);
        op.rhs.accept(self);
    }

    fn identifier(&mut self, _i: &str) {}
    fn literal(&mut self, _literal: &Literal) {}
    fn func_application(&mut self, _f: &FuncApplication<'a>) {}
    fn tuple(&mut self, _t: &Tuple<'a>) {}
    fn list(&mut self, _l: &Vec<Expression<'a>>) {}
}

impl<'a> Expression<'a> {
    pub fn accept<V: ExprVisitor<'a>>(&self, v: &mut V) {
        match self {
            Expression::BinOp(op) => v.bin_op(op),
            Expression::Identifier(i) => v.identifier(i),
            Expression::Literal(l) => v.literal(l),
            Expression::FuncApplication(f) => v.func_application(f),
            Expression::Tuple(t) => v.tuple(t),
            Expression::List(l) => v.list(l),
        }
    }
}

#[derive(Debug, Clone)]
pub struct FuncApplication<'a>(pub Vec<Expression<'a>>);

#[derive(Debug, Clone)]
pub struct Tuple<'a>(pub Vec<Expression<'a>>);

#[derive(Debug, Clone)]
pub enum Literal {
    Bool(bool),
    Float(f64),
    Int(i64),
    String(String),
}

#[derive(Clone)]
pub struct BinOp<'a> {
    pub op: Operator,
    pub lhs: Box<Expression<'a>>,
    pub rhs: Box<Expression<'a>>,
}

impl<'a> fmt::Debug for BinOp<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "BinOp<{:?}> {{ lhs: {:?}, rhs: {:?} }}", &self.op, &self.lhs, &self.rhs)
    }
}

#[derive(Debug, Clone)]
pub enum Operator {
    Star,
    Plus,
    Equals,
    NotEquals,
    GreaterThan,
    GreaterThanOrEqualTo,
    LessThan,
    LessThanOrEqualTo,
}

named!(pub expression<&str, Expression>, ws!(comparison));

macro_rules! operation {
    (
        do $name:ident
        after $secondary:ident
        operator (
            $( $sym:literal => $op:expr ),+
        )
    ) => {
        fn $name(input: &str) -> IResult<&str, Expression> {
            let input = ignore_whitespace(input);

            let (input, left) = $secondary(input)?;
            fold_many0(
                preceded(
                    take_while(is_whitespace),
                    pair(
                        alt((
                            // this is necessary because alt isn't implemented for 1-tuples
                            never_operator,
                            $(
                                map(tag($sym), |_| $op)
                            ),+
                        )),
                        $secondary
                    )
                ),
                left,
                |lhs, (op, rhs)| {
                    Expression::BinOp(BinOp {
                        op,
                        lhs: Box::new(lhs),
                        rhs: Box::new(rhs),
                    })
                }
            )(input)
        }
    };
}

fn never_operator(_: &str) -> IResult<&str, Operator> {
    // todo: clean this up?
    map(tag("_"), |_| Operator::Plus)("")
}

operation! {
    do comparison
    after addition
    operator (
        "==" => Operator::Equals,
        "!=" => Operator::NotEquals,
        ">=" => Operator::GreaterThanOrEqualTo,
        "<=" => Operator::LessThanOrEqualTo,
        ">" => Operator::GreaterThan,
        "<" => Operator::LessThan
    )
}

operation! {
    do addition
    after multiplication
    operator ("+" => Operator::Plus)
}

operation! {
    do multiplication
    after function_application
    operator ("*" => Operator::Star)
}

fn primary(input: &str) -> IResult<&str, Expression> {
    let input = ignore_whitespace(input);

    alt((
        literal_bool,
        literal_number,
        literal_string,
        identifier,
        list,
        tuple,
        delimited(tag("("), expression, tag(")")),
    ))(input)
}

fn tuple(input: &str) -> IResult<&str, Expression> {
    // ( ws* ) | ( expression , ws* ) | ( expression [, expression]*)
    let input = ignore_whitespace(input);

    let (input, _) = tag("(")(input)?;
    let (input, expr) = terminated(expression, tag(","))(input)?;
    let (input, mut list) = separated_list(tag(","), expression)(input)?;
    list.insert(0, expr);
    
    // check for optional trailing comma
    let tag_match: IResult<_, _> = tag(",")(input);
    let input = match tag_match {
        Ok((i, _)) => i,
        Err(_) => input
    };
    let input = ignore_whitespace(input);
    let (input, _) = tag(")")(input)?;

    Ok((input, Expression::Tuple(Tuple(list))))
}

fn list(input: &str) -> IResult<&str, Expression> {
    let input = ignore_whitespace(input);
    map(
        delimited(
            tag("["),
            separated_list(tag(","), expression), 
            tag("]"),
        ),
        |list| Expression::List(list)
    )(input)
}

fn function_application(input: &str) -> IResult<&str, Expression> {
    map(
        separated_nonempty_list(take_while1(is_whitespace), primary),
        |mut exprs| {
            if exprs.len() == 1 {
                exprs.pop()
                    .unwrap()
            } else {
                Expression::FuncApplication(FuncApplication(exprs))
            }
        }
    )(input)
}

fn literal_bool(input: &str) -> IResult<&str, Expression> {
    map(alt((tag("true"), tag("false"))), |val| {
        let val = if val == "true" { true } else { false };
        Expression::Literal(Literal::Bool(val))
    })(input)
}

fn literal_number(input: &str) -> IResult<&str, Expression> {
    map_res(take_while1(is_float), |val: &str| -> Result<Expression, ParseNumberError> {
        if val.contains('.') {
            let f = val.parse()?;
            Ok(Expression::Literal(Literal::Float(f)))
        } else {
            let f = val.parse()?;
            Ok(Expression::Literal(Literal::Int(f)))
        }
    })(input)
}

fn literal_string(input: &str) -> IResult<&str, Expression> {
    // TODO: Use escaped
    map_res(delimited(tag("\""), string_content, tag("\"")), |s: &str| -> Result<Expression, ParseNumberError> {
        Ok(Expression::Literal(Literal::String(s.to_string())))
    })(input)
}

fn identifier(input: &str) -> IResult<&str, Expression> {
    take_while_m_n(1, 1, is_leading_identifier)(input)?;

    map(take_while1(is_identifier), |val| {
        Expression::Identifier(val)
    })(input)
}

fn ignore_whitespace(input: &str) -> &str {
    use nom::error::ErrorKind;
    let (i, _) = take_while::<_, _, (&str, ErrorKind)>(is_whitespace)(input).unwrap();

    i
}

fn string_content(input: &str) -> IResult<&str, &str> {
    let is_text = |c: char| c.is_whitespace() || c.is_alphanumeric();
    take_while(is_text)(input)
}

fn is_whitespace(c: char) -> bool {
    c.is_whitespace()
}

fn is_float(c: char) -> bool {
    c.is_ascii_digit() || c == '.'
}

fn is_leading_identifier(c: char) -> bool {
    c.is_alphabetic() || c == '_'
}

fn is_identifier(c: char) -> bool {
    c.is_alphanumeric() || c == '_'
}
