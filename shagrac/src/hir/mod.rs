use crate::graph::Graph;
use crate::graph::library as nt;

use std::any::TypeId;

/*
struct VertexShader {

}

struct ShaderInputs {

}
*/

fn is_shader_program<T: 'static + ?Sized>(t: &T) -> bool {
    TypeId::of::<T>() == TypeId::of::<nt::ShaderProgram>()
}

fn analyze(graph: &Graph) -> Result<(), AnalysisError> {
    let program = graph.nodes.iter()
        .map(|(_, v)| v)
        .find(|v| is_shader_program(&*v.data));

    match program {
        Some(_) => Ok(()),
        None => Err(AnalysisError::MissingProgramNode)
    }
}

pub enum AnalysisError {
    MissingProgramNode
}
