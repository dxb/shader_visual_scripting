use crate::error::Error;
use crate::graph;
use crate::rpc;
use crate::rpc::{
    InitAction,
    Action,
    ServerResponse,
    ServerError,
};

use std::net::{TcpListener, TcpStream};

pub enum ExitStatus {
    /// Server received shutdown request followed by exit notification
    ReceivedShutdown,
    /// Server received an exit notification without a shutdown request
    Halted,
}

pub fn host() -> Result<i32, Error> {
    let listener = TcpListener::bind("127.0.0.1:62007")
        .expect("Failed to bind server to port");

    println!("Listening for client on port 62007.");

    match listener.accept() {
        Ok((stream, _)) => {
            println!("Connected to client.");
            let status = serve(stream)?;
            Ok(match status {
                ExitStatus::ReceivedShutdown => 0,
                ExitStatus::Halted => 1,
            })
        },
        Err(err) => Err(Error::IoError(err)),
    }
}

pub fn serve(mut stream: TcpStream) -> Result<ExitStatus, Error> {
    let mut buffer = Vec::new();

    loop {
        let action = rpc::expect_init(&mut buffer, &mut stream)?;

        match action {
            InitAction::Ignore => {},
            InitAction::Exit => return Ok(ExitStatus::Halted),
            InitAction::Error(id, err) => {
                rpc::send_error(&mut buffer, &mut stream, id, err)?;
            }
            InitAction::Proceed(id) => {
                rpc::send_response(&mut buffer, &mut stream, id, ServerResponse::Initialized)?;
                break;
            },
        }
    }

    loop {
        let action = rpc::handle_rpc(&mut buffer, &mut stream)?;

        match action {
            Action::NotificationIgnored => {}
            Action::Error(id, err) => {
                rpc::send_error(&mut buffer, &mut stream, id, err)?;
            },
            Action::OpenFile(id, path) => {
                match graph::load(&path) {
                    Ok((_, edits)) => {
                        rpc::send_response(&mut buffer, &mut stream, id, ServerResponse::UpdateGraph(edits))?;
                    },
                    Err(err) => {
                        let msg = format!("File system error: {}", err);
                        rpc::send_error(&mut buffer, &mut stream, id, ServerError::FileSystemError(msg))?;
                    }
                }
                rpc::send_response(&mut buffer, &mut stream, id, ServerResponse::Initialized)?;
            }
        }
    }

    Ok(ExitStatus::Halted)
}

    /*
    // Wait for handshake
    if let Request::Init = receive_message(&mut buffer, &mut stream)? {
        send_message(&mut buffer, &mut stream, &Response::Init)?;
        let ack = receive_message(&mut buffer, &mut stream)?;
        if let Request::Ack = ack {
            // ...
        } else {
            return Err(Error::ProtocolError("Expected ack following init".to_string()));
        }
    } else {
        return Err(Error::ProtocolError("Expected init message".to_string()));
    }

    let mut running = true;
    while running {
        running = handle_rpc(&mut buffer, &mut stream)?;
    }
    Ok(())
}*/

/*
/// Wait for a remote procedure call and then handle it
fn handle_rpc<S: Read + Write>(buffer: &mut Vec<u8>, stream: &mut S) -> Result<bool, Error> {
    let call = match receive_message(buffer, stream) {
        Ok(r) => r,
        Err(Error::ParseError(p)) => {
            let message = format!("{}", p);
            println!("Received an invalid message:\n{}", &p);
            let response = Response::Error { message, error_kind: ErrorKind::Json };
            send_message(buffer, stream, &response)?;
            return Ok(true)
        },
        err => err?,
    };
    let (response, keep_open) = call.respond();
    let response = response.map(|a| Response::from(a))
        .or_else(|err| -> Result<Response, Error>
            { Ok(Response::Error { message: err, error_kind: ErrorKind::Protocol }) }
        )
        .and_then(|response| {
            send_message(buffer, stream, &Response::Ack)?;
            Ok(response)
        })?;
    send_message(buffer, stream, &response)?;
    match receive_message(buffer, stream)? {
        Request::Ack => {},
        _ => return Err(Error::ProtocolError("Expected an ack following result".to_string())),
    }
    Ok(keep_open)
}*/

/*
#[derive(Debug, Serialize)]
#[serde(tag = "type")]
enum Response {
    None,
    Init,
    Ack,
    Error { message: String, error_kind: ErrorKind },
    LoadGraph { graph: Graph },
}

impl From<Action> for Response {
    fn from(action: Action) -> Response {
        match action {
            Action::None => Response::None,
            Action::LoadGraph { graph } => Response::LoadGraph { graph },
        }
    }
}

#[derive(Debug, Serialize)]
enum Action {
    None,
    LoadGraph { graph: Graph },
}

impl Action {
    fn none() -> Result<Self, String> {
        Ok(Action::None)
    }
}

#[derive(Debug, Serialize)]
enum ErrorKind {
    Protocol,
    Json,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "type")]
enum Request {
    Init,
    // I think this will need to carry more information...
    Ack,
    Close,
    OpenFile { path: String },
}

impl Request {
    fn respond(&self) -> (Result<Action, String>, bool) {
        let res = match self {
            Request::Close => return (Action::none(), false),

            Request::Init => Err("Init message is invalid after handshake".to_string()),
            Request::Ack => Err("Ack is invalid as an RPC".to_string()),

            Request::OpenFile { path } => {
                node::load_graph(path)
                    .map(|graph| Action::LoadGraph { graph })
                    .map_err(|err| format!("{}", err))
            },
        };
        (res, true)
    }
}
*/
