use serde::{Serialize, Deserialize};
use serde_json::Error as SerdeError;

use std::error;
use std::fmt;
use std::io;

/* Shagrac Error */

#[derive(Debug)]
pub enum Error {
    IoError(io::Error),
    ParseError(SerdeError),
    ProtocolError(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::IoError(err) => write!(f, "{}", err),
            Error::ParseError(err) => write!(f, "{}", err),
            Error::ProtocolError(err) => write!(f, "An invalid message was transmitted: {}", err),
        }
    }
}

impl error::Error for Error {}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::IoError(err)
    }
}

impl From<SerdeError> for Error {
    fn from(err: SerdeError) -> Self {
        Error::ParseError(err)
    }
}

/* ParseNumberError */

#[derive(Debug)]
pub enum ParseNumberError {
    Float(std::num::ParseFloatError),
    Int(std::num::ParseIntError),
}

impl fmt::Display for ParseNumberError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParseNumberError::Float(err) => write!(f, "{}", err),
            ParseNumberError::Int(err) => write!(f, "{}", err),
        }
    }
}

impl error::Error for ParseNumberError {}

impl From<std::num::ParseFloatError> for ParseNumberError {
    fn from(err: std::num::ParseFloatError) -> Self {
        ParseNumberError::Float(err)
    }
}

impl From<std::num::ParseIntError> for ParseNumberError {
    fn from(err: std::num::ParseIntError) -> Self {
        ParseNumberError::Int(err)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum NodeError {
    InvalidExpr,
}

impl fmt::Display for NodeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            NodeError::InvalidExpr => write!(f, "Expression contains a syntax error."),
        }
    }
}
