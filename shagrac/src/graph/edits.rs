use crate::graph::core::{NodeId, GraphPos};
use crate::graph::library::NodeData;

use serde::{Serialize, Deserialize};

use std::borrow::Borrow;

#[derive(Debug, Serialize, Deserialize)]
pub struct GraphEdits {
	pub version: Option<VersionId>,
	pub edge_edits: Vec<EdgeEdit>,
	pub node_edits: Vec<NodeEdit>,
}

pub type VersionId = u32;

#[derive(Debug, Serialize, Deserialize)]
#[serde(from = "StorageEdit")]
pub enum EdgeEdit {
	Create {
        head: (NodeId, String),
        tail: (NodeId, String),
    },
}

impl From<StorageEdit> for EdgeEdit {
    fn from((head, tail): StorageEdit) -> Self {
        EdgeEdit::Create {
            head,
            tail
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum NodeEdit {
	Create {
        id: NodeId,
        position: GraphPos,
        node: NodeDescriptor,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NodeDescriptor {
    name: String,
    expr: Option<String>,
    inputs: Vec<String>,
    outputs: Vec<String>,
    errors: Vec<String>,
}

impl NodeDescriptor {
    pub fn from_data(data: &NodeData) -> NodeDescriptor {
        let inputs = <_ as Borrow<Vec<&str>>>::borrow(&data.inputs())
            .iter()
            .map(|s| s.to_string())
            .collect();
        let outputs = <_ as Borrow<Vec<&str>>>::borrow(&data.outputs())
            .iter()
            .map(|s| s.to_string())
            .collect();
        let errors = data.errors()
            .iter()
            .map(|err| format!("{}", err))
            .collect();
        NodeDescriptor {
            name: data.name().to_string(),
            expr: data.expr().map(str::to_string),
            inputs,
            outputs,
            errors,
        }
    }
}

type StorageEdit = ((NodeId, String), (NodeId, String));
