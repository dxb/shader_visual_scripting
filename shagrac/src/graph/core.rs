use crate::graph::library::NodeData;

use serde::Deserialize;

pub type NodeId = u32;
pub type GraphPos = [i32; 2];

#[derive(Debug, Deserialize)]
pub struct Node {
    pub id: NodeId,
    pub position: GraphPos,
    #[serde(flatten)]
    pub data: Box<dyn NodeData>,
}

pub struct Edge {
    head: (NodeId, String),
    tail: (NodeId, String),
}
