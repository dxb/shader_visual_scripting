use crate::analysis;
use crate::error::NodeError;

use serde::{Serialize, Deserialize};

use lazy_static::lazy_static;

use std::borrow::Cow;
use std::fmt;

#[typetag::serde(tag = "type", content = "data")]
pub trait NodeData: fmt::Debug {
    fn name(&self) -> &'static str;
    fn expr(&self) -> Option<&str> { None }
    // TODO: use []?
    fn inputs(&self) -> Cow<Vec<&str>>;
    fn outputs(&self) -> Cow<Vec<&str>>;
    fn errors(&self) -> Cow<[NodeError]> { Cow::from(Vec::new()) }
    fn resolve(&mut self) {}
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ShaderProgram;
#[derive(Debug, Serialize, Deserialize)]
pub struct VertexAttributes;
#[derive(Debug, Serialize, Deserialize)]
pub struct Expression {
    expr: String,
    #[serde(skip)]
    errors: Vec<NodeError>,
    #[serde(skip)]
    inputs: Vec<String>,
}

macro_rules! static_sockets {
    ($name:ident, $e:expr) => {
        fn $name(&self) -> Cow<Vec<&str>> {
            lazy_static! {
                static ref SOCKETS: Vec<&'static str> = { $e };
            }
            Cow::Borrowed(&*SOCKETS)
        }
    };
}

#[typetag::serde]
impl NodeData for ShaderProgram {
    fn name(&self) -> &'static str { "Shader Program" }
    static_sockets!(inputs, vec!["position", "fragments"]);
    static_sockets!(outputs, vec![]);
}

#[typetag::serde]
impl NodeData for VertexAttributes {
    fn name(&self) -> &'static str { "Vertex Attributes" }
    static_sockets!(inputs, vec!["list"]);
    static_sockets!(outputs, vec![]);
}

#[typetag::serde]
impl NodeData for Expression {
    fn name(&self) -> &'static str { "Expression" }
    fn expr(&self) -> Option<&str> { Some(&self.expr) }
    fn inputs(&self) -> Cow<Vec<&str>> {
        Cow::Owned(self.inputs.iter().map(|s| s.as_str()).collect())
    }
    static_sockets!(outputs, vec!["result"]);
    fn errors(&self) -> Cow<[NodeError]> {
        Cow::Borrowed(self.errors.as_slice())
    }
    fn resolve(&mut self) {
        self.inputs.clear();
        let result = analysis::expr_inputs(&self.expr);
        match result {
            Ok(vars) => {
                for var in vars.into_iter() {
                    self.inputs.push(var);
                }
                self.inputs.sort();
            },
            Err(err) => {
                self.errors.clear();
                self.errors.push(err);
            }
        }
    }
}
