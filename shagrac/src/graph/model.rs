//! Some nodes are flex nodes (or "dynamic"), some are static (e.g. "ShaderProgram").
//!
//! Flex nodes are any nodes whose sockets change based on their connection.
//!
//! Some nodes are entirely dependent on their internal expression content,
//! some others depend on the connections that are made into them, some depend on
//! the actual data that goes through them, requiring a report from the HIR analysis.

use crate::graph::core::{NodeId, Node, Edge};
use crate::graph::edits::*;
use crate::error::Error;

use serde::Deserialize;

use std::cmp::max;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::collections::HashMap;

pub struct Graph {
    pub version: u32,
    pub next_id: NodeId,
    pub nodes: HashMap<NodeId, Node>,
    pub edges: Vec<Edge>,
}

impl Graph {
    fn apply_edge_edit(&mut self, _edit: &EdgeEdit) {
        /*
        match edit {
            // TODO
            EdgeEdit::Create { tail: (tail, tail_socket), head: (head, head_socket) } => {
                // for the sockets to be valid we must resolve the node's first

            }
        }*/
    }
}

#[derive(Debug, Deserialize)]
struct StorageModel {
    nodes: HashMap<NodeId, Node>,
    edges: Vec<EdgeEdit>,
}

pub fn load<P: AsRef<Path>>(path: P) -> Result<(Graph, GraphEdits), Error> {
    let mut r = BufReader::new(File::open(path)?);
    let StorageModel {
        mut nodes,
        edges,
    } = serde_json::from_reader(&mut r)?;
    // resolve all the nodes
    for (_, n) in &mut nodes {
        n.data.resolve();
    }
    let next_id = nodes.iter().fold(0, |next, (consider, _)| max(next, consider + 1));
    let version = 0;
    let node_edits = nodes.iter()
        .map(|(_, v)|
            NodeEdit::Create {
                id: v.id,
                position: v.position,
                node: NodeDescriptor::from_data(&*v.data)
            }
        )
        .collect();
    let mut graph = Graph {
        version,
        next_id,
        nodes,
        edges: Vec::new(),
    };
    for edge in &edges {
        graph.apply_edge_edit(edge);
    }
    let edits = GraphEdits {
        version: Some(version),
        node_edits,
        edge_edits: Vec::new(), // TODO: use edges
    };
    Ok((graph, edits))
}

/*
use lazy_static::lazy_static;
use serde::{Serialize, Deserialize};

use std::fmt;

#[derive(Debug, Deserialize, Serialize)]
pub struct Graph {
    nodes: HashMap<NodeId, Node>,
    /// Edges are keyed by head since the graph tends to follow arrows backwards
    edges: HashMap<NodeId, Edge>,
}
pub type NodeId = u32;

#[derive(Debug, Deserialize, Serialize)]
struct Node {
    pos: [f32; 2],
    data: Box<dyn NodeData>,
}

#[derive(Debug, Deserialize, Serialize)]
struct Edge {
    tail: NodeId,
    /// The pointy bit/destination part of the arrow
    head: NodeId,
}

#[typetag::serde(tag = "type")]
trait NodeData: fmt::Debug {
    /// Determine inputs and outputs
    fn resolve(&mut self) {}

    fn typecheck(&self, _output: &str, _expected: SocketType) {
        panic!("This node has no outputs to typecheck");
    }

    fn inputs(&self) -> &[Socket];
}

struct Socket {
    name: String,
    kind: SocketType,
}

impl Socket {
    fn statik<S: ToString>(name: S, kind: ExprType) -> Self {
        Socket {
            name: name.to_string(),
            kind: SocketType {
                class: ExprClass::RuntimeExpr,
                kind: kind,
            }
        }
    }
}

struct SocketType {
    class: ExprClass,
    kind: ExprType,
}

enum ExprClass {
    RuntimeExpr,
    MetaExpr,
    ConstantExpr,
}

enum ExprType {
    Any,
    Vector { size: u32, width: u32 },
}

/// Basic shader program node, with position and fragment outputs.
#[derive(Debug, Deserialize, Serialize)]
struct ShaderProgram;

#[typetag::serde]
impl NodeData for ShaderProgram {
    fn inputs(&self) -> &[Socket] {
        lazy_static! {
            static ref SOCKETS: [Socket; 2] = {
                [
                    Socket::statik("position", ExprType::Vector { size: 4, width: 32 }),
                    Socket::statik("position", ExprType::Any)
                ]
            };
        }
        &*SOCKETS
    }
}

/*
pub fn analyze(graph: &Graph) {

}
*/
*/
