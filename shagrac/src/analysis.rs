use crate::expr::{Expression, ExprVisitor, FuncApplication, Tuple, expression};
use crate::error::NodeError;

use std::collections::HashSet;

struct IdentifierCollector { inner: HashSet<String> }

impl IdentifierCollector {
    fn new() -> Self {
        IdentifierCollector {
            inner: HashSet::new(),
        }
    }

    fn eval(self) -> HashSet<String> {
        self.inner
    }
}

impl<'a> ExprVisitor<'a> for IdentifierCollector {
    fn identifier(&mut self, i: &str) {
        // this actually *could* borrow i, I think! but must add a few lifetimes
        self.inner.insert(i.to_string());
    }

    fn func_application(&mut self, f: &FuncApplication<'a>) {
        for e in &f.0 {
            e.accept(self);
        }
    }

    fn tuple(&mut self, t: &Tuple<'a>) {
        for e in &t.0 {
            e.accept(self);
        }
    }

    fn list(&mut self, l: &Vec<Expression<'a>>) {
        for e in l {
            e.accept(self);
        }
    }
}

pub fn expr_inputs(expr: &str) -> Result<HashSet<String>, NodeError> {
    expression(expr)
        .or_else(|err| {
            Err(err)
        })
        .map_err(|_| NodeError::InvalidExpr)
        .and_then(|(remaining, e)| {
            if remaining.trim() != "" {
                return Err(NodeError::InvalidExpr)
            }
            let mut v = IdentifierCollector::new();
            e.accept(&mut v);
            Ok(v.eval())
        })
}
