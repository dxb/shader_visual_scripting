extern crate shagrac;

use shagrac::debug_expression;
use std::io;
use std::io::Write;
use std::env;
use std::process;

fn main() {
    let mut args = env::args().skip(1);
    if let Some(flag) = args.next() {
        if flag == "--server" {
            let status = shagrac::server::host()
                .expect("The server encountered a fatal error");
            process::exit(status);
        } else {
            println!("Found unknown arg! {}", flag);
            process::exit(1);
        }
    }

    let stdin = io::stdin();

    loop {
        println!("Enter a string to tokenize!");
        let mut expr = String::new();
        print!("> ");
        io::stdout().flush()
            .unwrap();
        stdin.read_line(&mut expr)
            .unwrap();

        let (input, parsed) = match debug_expression(&expr) {
            Ok(r) => r,
            Err(err) => {
                println!("Syntax error: {:?}", err);
                continue;
            }
        };

        if input.trim().len() > 0 {
            println!("Input not parsed: {:?}", input.trim());
        }
        println!("Expr:\n\t{}", parsed);
    }
}
