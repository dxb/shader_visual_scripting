/// This will contain features required by the shader to establish minimum versions.
struct Requirements;

struct VertexShader {
    requirements: Requirements,
    attributes: Vec<ShaderInput>,
    position: Expression,
}

struct ShaderInput {
    name: String,
    type: ValueType,
}

enum ValueType {
    Vec4
}

pub fn compile() -> (String, String) {
    let vertex_shader = r#"
        #version 330 core

        in vec4 pos;
        in vec4 color;

        out vec4 v_color;

        void main(void) {
            gl_Position = pos;
            v_color = color;
        }
    "#;
    let fragment_shader = r#"
        #version 330 core

        in vec4 v_color;

        out vec4 r_frag_color;

        void main(void) {
            r_frag_color = v_color;
        }
    "#;
    (vertex_shader.to_string(), fragment_shader.to_string())
}
