use crate::error::Error;
use crate::graph::edits::GraphEdits;

use serde_json::Value;
use serde::{Serialize, Deserialize};

use std::io::{Read, Write};
use std::borrow::Cow;

enum RpcMessage<'buf> {
    Req(RpcRequest<'buf>),
    Res(RpcResponse<'buf>),
}

impl<'buf> From<RpcRequest<'buf>> for RpcMessage<'buf> {
    fn from(r: RpcRequest<'buf>) -> RpcMessage<'buf> {
        RpcMessage::Req(r)
    }
}

impl<'buf> From<RpcResponse<'buf>> for RpcMessage<'buf> {
    fn from(r: RpcResponse<'buf>) -> RpcMessage<'buf> {
        RpcMessage::Res(r)
    }
}

type RpcId = f32;

#[derive(Debug, Deserialize, Serialize)]
struct RpcRequest<'buf> {
    jsonrpc: &'buf str,
    method: &'buf str,
    params: Option<Value>,
    id: Option<RpcId>,
}

#[derive(Debug, Deserialize, Serialize)]
struct RpcResponse<'buf> {
    jsonrpc: &'buf str,
    result: Option<Value>,
    error: Option<RpcError>,
    id: RpcId,
}

#[derive(Debug, Deserialize, Serialize)]
struct RpcError {
    code: i32,
    message: String,
}

pub enum InitAction {
    Proceed(RpcId),
    Error(RpcId, ServerError),
    Ignore,
    Exit
}

pub enum Action {
    Error(RpcId, ServerError),
    OpenFile(RpcId, String),
    NotificationIgnored,
}

macro_rules! param_typecheck {
    (
        $id:expr,
        $params:expr,
        $(
            $p:ident : $t:ty
        ),+
    ) => {{
        type T = (
            $($t),+,
        );
        const MSG: &str = concat!("{ ", $( stringify!($p), ": ", stringify!($t) ),+ , " }");
        fn tc(params: Option<serde_json::Value>) -> Option<T> {
            // if let, params is none
            if let Some(params) = params {
                Some((
                    $(
                        serde_json::from_value(params.get(stringify!($p))?.clone())
                            .ok()?
                    ),+,
                ))
            } else {
                None
            }
            // type check each into a tuple
        }
        match tc($params) {
            Some(t) => t,
            None => return Ok(Action::Error($id, ServerError::InvalidParams(MSG.to_string()))),
        }
    }};
}

pub enum ServerResponse {
    Initialized,
    UpdateGraph(GraphEdits),
}

pub enum ServerError {
    ServerNotInitialized,
    InvalidParams(String),
    InvalidRequest,
    MethodNotFound,

    FileSystemError(String),
}

pub fn expect_init<R: Read>(buffer: & mut Vec<u8>, input: &mut R) -> Result<InitAction, Error> {
    let message = receive_message(buffer, input)?;
    match message {
        RpcMessage::Req(RpcRequest { method: "initialize", id: Some(id), .. })
            => Ok(InitAction::Proceed(id)),
        RpcMessage::Req(RpcRequest { id: Some(id), ..})
            => Ok(InitAction::Error(id, ServerError::ServerNotInitialized)),
        RpcMessage::Req(RpcRequest { method, id: None, .. }) => {
            // all notifications should be dropped except for exit notification
            Ok(
                if method == "exit" {
                    InitAction::Exit
                } else {
                    InitAction::Ignore
                }
            )
        }
        RpcMessage::Res(..) => {
            Err(Error::ProtocolError("Server received a response before sending any requests".to_string()))
        },
    }
}

pub fn handle_rpc<R: Read>(buffer: &mut Vec<u8>, input: &mut R) -> Result<Action, Error> {
    let message = receive_message(buffer, input)?;
    match message {
        RpcMessage::Req(RpcRequest { id: Some(id), method, params, jsonrpc: "2.0" }) => {
            // Request
            match method {
                "open_file" => {
                    let (path, ) = param_typecheck!(id, params, path : String);
                    Ok(Action::OpenFile(id, path))
                },
                _ => {
                    Ok(Action::Error(id, ServerError::MethodNotFound))
                }
            }
        },
        RpcMessage::Req(RpcRequest { id: None, jsonrpc: "2.0", .. }) => {
            // Notification
            Ok(Action::NotificationIgnored)
        },
        RpcMessage::Req(RpcRequest { id, .. }) => {
            // catches weird jsonrpc values
            if let Some(id) = id {
                Ok(Action::Error(id, ServerError::InvalidRequest))
            } else {
                // TODO: Respond to invalid notifications
                Ok(Action::NotificationIgnored)
            }
        },
        RpcMessage::Res(..) => {
            Err(Error::ProtocolError("Server received a response before sending any requests".to_string()))
        },
    }
}

fn receive_message<'b, R: Read>(buffer: &'b mut Vec<u8>, input: &mut R) -> Result<RpcMessage<'b>, Error> {
    let mut content_length_buffer = [0u8; 8];
    input.read_exact(&mut content_length_buffer)?;
    assert_eq!(content_length_buffer.len(), 8);
    let content_length = usize::from_le_bytes(content_length_buffer);

    buffer.resize(content_length, 0);
    input.read_exact(buffer)?;

    let first_parse = serde_json::from_slice::<RpcRequest>(buffer)
            .map(|req| RpcMessage::from(req))
            .map_err(|err| err.into());
    if let Err(_) = first_parse {
        serde_json::from_slice::<RpcResponse>(buffer)
            .map(|res| RpcMessage::from(res))
            .map_err(|err| err.into())
    } else {
        first_parse
    }
}

pub fn send_response<W: Write>(buffer: &mut Vec<u8>, output: W, id: f32, response: ServerResponse) -> Result<(), Error> {
    match response {
        ServerResponse::Initialized => send_message(buffer, output, id, Ok(true)),
        ServerResponse::UpdateGraph(edits) => send_message(buffer, output, id, Ok(edits)),
    }
}

pub fn send_error<W: Write>(buffer: &mut Vec<u8>, output: W, id: f32, err: ServerError) -> Result<(), Error> {
    send_message::<(), _>(buffer, output, id, Err(err))
}

fn send_message<T: Serialize, W: Write>(mut buffer: &mut Vec<u8>, mut output: W, id: f32, message: Result<T, ServerError>) -> Result<(), Error> {
    // TODO: If we make RpcResponse generic over T, could we get any performance gains?
    let res = match message {
        Ok(r) => {
            RpcResponse {
                jsonrpc: "2.0",
                error: None,
                result: Some(serde_json::to_value(r)?),
                id,
            }
        },
        Err(err) => {
            RpcResponse {
                jsonrpc: "2.0",
                error: Some(RpcError::from(err)),
                result: None,
                id,
            }
        }
    };
    buffer.clear();
    serde_json::to_writer(&mut buffer, &res)?;
    let content_length_buffer = buffer.len().to_le_bytes();
    assert_eq!(content_length_buffer.len(), 8);
    output.write_all(&content_length_buffer)?;
    output.write_all(&buffer)?;
    output.flush()?;
    Ok(())
}

impl From<ServerError> for RpcError {
    fn from(code: ServerError) -> Self {
        RpcError {
            code: code.code(),
            message: code.message().to_string(),
        }
    }
}

const INVALID_REQUEST: i32 = -32600;
const SERVER_NOT_INITIALIZED: i32 = -32002;
const INVALID_PARAMS: i32 = -32602;
const METHOD_NOT_FOUND: i32 = -32601;

const FILE_SYSTEM_ERROR: i32 = 1000;

/*
const ParseError: i32 = -32700;
const InternalError: i32 = -32603;
const serverErrorStart: i32 = -32099;
const serverErrorEnd: i32 = -32000;
const UnknownErrorCode: i32 = -32001;
*/

impl ServerError {
    fn code(&self) -> i32 {
        match self {
            ServerError::ServerNotInitialized => SERVER_NOT_INITIALIZED,
            ServerError::InvalidParams(..) => INVALID_PARAMS,
            ServerError::InvalidRequest => INVALID_REQUEST,
            ServerError::MethodNotFound => METHOD_NOT_FOUND,

            ServerError::FileSystemError(..) => FILE_SYSTEM_ERROR,
        }
    }

    fn message(&self) -> Cow<str> {
        match self {
            ServerError::ServerNotInitialized => Cow::Borrowed("Request received before server initialized"),
            ServerError::InvalidParams(message) => {
                let mut buffer = "Invalid parameters, expected:".to_string();
                buffer.push_str(message);
                Cow::Owned(buffer)
            },
            ServerError::InvalidRequest => Cow::Borrowed("Received an invalid request"),
            ServerError::MethodNotFound => Cow::Borrowed("Method not found"),

            ServerError::FileSystemError(s) => Cow::Borrowed(s),
        }
    }
}

/*

fn send_notification() {

}

fn send_request() {

}

enum ClientRequest {
    Initialize,
    OpenFile(String),
}

enum ServerResponse {
    Initialized,
    GraphEdits(GraphEdits),
}
*/
